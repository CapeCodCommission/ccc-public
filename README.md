# CCC Public Repo for External Support

> A repo for external purposes for apps in development by CCC dev team.

## CCC SHORELINE INTERVENTION APP
``` bash

# switch to ccc-shoreline-intervention-working branch
git checkout ccc-shoreline-intervention-working

# install dependencies
npm install

# serve with hot reload at localhost:4000/#/
npm run dev
```
